import axios from "axios";
import qs from "qs";

// axios.defaults.baseURL = ''  //正式
// axios.defaults.baseURL = 'http://test' //测试

//post请求头
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded;charset=UTF-8";
axios.defaults.headers.get["Access-Control-Expose-Headers"] = "X-Cache-Webcdn";
// axios.defaults.headers.common = {
//     'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
//     'X-Requested-With': 'XMLHttpRequest'
// };

//设置超时
axios.defaults.timeout = 10000;

axios.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    if (response.status == 200) {
      return Promise.resolve(response);
    } else {
      return Promise.resolve(response);
    }
  },
  (error) => {
    // ElNotification({
    //     showClose: false,
    //     message: `异常请求：${JSON.stringify(error.message)}`,
    //     type: "error",
    //     duration: 3000,
    //     position: "bottom-right",
    // });
    console.log(error);
  }
);
export default {
  post(url, data) {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        headers: {
          authenticate: "Access-Control-Allow-Origin",
        },
        url,
        data: qs.stringify(data),
      })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  get(url, data) {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        headers: {
          authenticate: "Access-Control-Allow-Origin",
        },
        url,
        params: data,
      })
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
