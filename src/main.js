import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// import axios from './store/axios';
import TDesign from "tdesign-vue";
// 引入组件库全局样式资源
import "tdesign-vue/es/style/index.css";
import * as echarts from "echarts";
import storage from "./packaging/storage";
import "bootstrap/dist/css/bootstrap.css";

Vue.use(TDesign);
Vue.config.productionTip = false;
Vue.prototype.$echarts = echarts;
// Vue.config.globalProperties.$http = axios;

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  //console.log("后面", to, from);
  if (from.name != null) {
    storage.storageSet("list", to.name);
  }
  next();
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
