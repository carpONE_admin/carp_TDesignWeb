function storageSet(key, value) {
  localStorage.setItem(key, JSON.stringify(value));
}

function storageGet(key) {
  return JSON.parse(localStorage.getItem(key));
}

// function storageRemove() {
//     localStorage.removeItem(key)
// };

export default {
  storageSet,
  storageGet,
};
