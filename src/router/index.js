import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "/",
    meta: {
      title: "首页",
    },
    component: () => import("../views/Main.vue"),
    children: [
      {
        path: "/home",
        name: "home",
        meta: {
          title: "首页",
        },
        component: () => import("../views/Home/Home.vue"),
      },
      {
        path: "/user-circle",
        name: "user-circle",
        meta: {
          title: "个人中心",
        },
        component: () => import("../views/persional/Persional.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

export default router;
