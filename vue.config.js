module.exports = {
  // 基本路径
  publicPath: "/",
  // 输出目录
  outputDir: "dist",
  // 用于嵌套生成的静态资产（js,css，img，fonts）的目录
  assetsDir: "assets",
  // 生产环境SourceMap
  productionSourceMap: true,
  devServer: {
    port: 2023,
    open: true,
    host: "0.0.0.0",
    // proxy: {} //设置代理
    proxy: {
      "": {
        target: "http://127.0.0.1:8000",
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          "": "",
        },
      },
    },
  },
};
